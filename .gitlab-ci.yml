image: debian:testing

stages:
  - lint
  - build
  - test

.python_versions: &python-versions
  - '3.7'
  - '3.8'
  - '3.9'
  - '3.10'
  - '3.11'
  - '3.12'
  - '3.13'
  # - rc

cache:
  paths:
    - build
    - '*.egg-info'

build:
  stage: build
  needs: []
  parallel:
    matrix:
      - PYTHON: *python-versions
  image: python:$PYTHON-slim-bookworm
  script:
  - pip install setuptools
  - ./setup.py build
  - ./setup.py sdist bdist
  artifacts:
    paths:
      - dist/*

verify-tag:
  stage: lint
  needs: []
  only:
  - tags
  script:
  - grep -F "version = $CI_COMMIT_TAG" setup.cfg

typing:
  stage: lint
  needs: []
  parallel:
    matrix:
      - PYTHON: *python-versions
  image: python:$PYTHON-slim-bullseye
  script:
    # We may need a compiler when running on the RC
  - test $PYTHON != rc || (
      apt update &&
      apt install -y --no-install-recommends
      python3-all-dev build-essential git &&
      pip install git+https://github.com/python/typed_ast@adfe297
    )
  - pip install mypy
  - pip install .
  - mypy --junit-xml=mypy.xml pristine_lfs
  artifacts:
    reports:
      junit: mypy.xml

test:
  stage: test
  needs: []
  parallel:
    matrix:
      - PYTHON: *python-versions
  image: python:$PYTHON-slim-bullseye
  script:
  - apt update
  - apt install -y --no-install-recommends
    git git-lfs
  - pip install setuptools pytest
  - pip install .
  - pytest --junit-xml=test-results.xml
  artifacts:
    reports:
      junit: test-results.xml

codestyle:
  stage: lint
  needs: []
  allow_failure: true
  script:
  - apt update
  - apt install -y flake8
    python3-flake8-docstrings
    python3-flake8-blind-except
    python3-flake8-builtins
  - flake8 --verbose
